This module provides integration with the Mailigen email delivery service.

mailigen.module provides provides basic configuration and API integration.
Specific functionality is provided by a set of submodules that depend upon
mailigen.module. See their respective README's for more details.

## Features
  * API integration
  * Support for an unlimited number of mailing lists
  * Having an anonymous sign up form to enroll users in a general newsletter.
  * Each Mailigen list can be assigned to one or more roles
  * Editing of user list subscriptions on the user's edit page
  * Allow users to subscribe during registration
  * Map token and profile values to your Mailigen merge fields
  * Required, optional, and free form list types.
  * Standalone subscribe and unsubscribe forms
  * Subscriptions can be maintained via cron or in real time
  * Individual blocks for each newsletter

## Installation Notes
  * You need to have a Mailigen API Key.
  * You need to have at least one list created in Mailigen to use the
    mailigen_list module.
  * The MGAPI library must be copied into your libraries folder. 
    It's available at http://www.mailigen.com/assets/files/api/mgapi.zip
    The only required files are MGAPI.class.php.
  * You must have at least version 7.x-2.0 of the libraries module installed.


## Configuration
  1. Direct your browser to http://example.com/admin/config/services/mailigen
  to configure the module.

  2. You will need to put in your Mailigen API key for your Mailigen account.
  If you do not have a Mailigen account, go to
  [http://www.mailigen.com]([http://www.mailigen.com) and sign up for a new
  account. Once you have set up your account and are logged into your account,
  Select "API keys" from the Account dropdown menu.

  3. Click Add a Key.
  Copy your newly create API key and go to the
  [Mailigen config](http://example.com/admin/config/services/mailigen) page in
  your Drupal site and paste it into the Mailigen API Key field.
  Batch limit - Maximum number of users to process in a single cron run.
  Use Secure Connection - Communicate with the Mailigen API over a secure connection.

## Submodules
  * mailigen_lists: Synchronize Drupal users with Mailigen lists and allow
    users to subscribe, unsubscribe, and update member information.
