Synchronize Drupal users with Mailigen lists and allow users to subscribe, 
unsubscribe, and update member information. This module requires the
[Entity module](http://www.drupal.org/project/entity).

## Installation

1. Enable the Mailigen Lists module and the Entity Module

2. To use Mailigen Lists module, you will need to install and enable the Entity
API module [http://drupal.org/project/entity]([http://drupal.org/project/entity)

3. If you haven't done so already, add a list in your Mailigen account. Follow 
these directions provided by Mailigen on how to 
[Create a new email list](https://admin.mailigen.com/help/getting_started?id=1)

4. Direct your browser to: http://example.com/admin/config/services/mailigen 
You will now see a "Lists and Users" tab. http://example.com/admin/config/services/mailigen/lists


## Usage

Adding a list - click "Add a List". List settings vary depending on the type of 
list being created. Lists are exportable and have a machine name, so play
nice with features or your own codified configurations.

### Required lists

Required lists are automatically synchronized with the sites users. You can 
choose the following settings for the required lists:

* Sync List During Cron
If this is set, users will be subscribed to the required list during cron runs. 
If you do not select this option, subscription will take place when a user is 
added/edited.


### Optional lists

Optional lists provide a checkbox allowing users to subscribe during 
registration or when updating their account. They have the following settings:

* Require subscribers to Double Opt-in
New subscribers will be sent a link with an email from Mailigen that they must 
follow to confirm their subscription. 

* Show subscription options on the user registration form.
This will only apply for lists granted to an authenticated role. 

* Show Subscription Options on User Edit Screen
If set, a tab will be presented for managing newsletter subscriptions when 
editing an account. Here the user can subscribe and unsubscribe from the 
Mailigen lists to which they belong.

Creating an Optional list will provide you with a block called Mailigen 
Subscription Form: [list title].

### Free form lists 

This is the only type of list allowed for the Anonymous role and has the 
following options.

* Require subscribers to Double Opt-in
New subscribers will be sent a link with an email from Mailigen that they must 
follow to confirm their subscription.

Creating an Free From list will provide you with a block called Mailigen 
Subscription Form: [list title]. This block contains a signup form with all 
Mailigen merge fields displayed. 

Default values are allowed for authenticated users based on token mappings. 
There for if you map First Name to username, the module will fill in that 
information from the database.
