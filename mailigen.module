<?php

/**
 * @file
 * Mailigen module.
 */

/**
 * Implements hook_libraries_info().
 */
function mailigen_libraries_info() {
  $libraries['mailigen'] = array(
    'name' => 'Mailigen MGAPI',
    'vendor url' => 'http://dev.mailigen.com/',
    'download url' => 'http://api.mailigen.com/1.0/download/mgapi.zip',
    'version' => '1.0',
    'files' => array(
      'php' => array('MGAPI.class.php'),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_menu().
 */
function mailigen_menu() {
  $items = array();

  $items['admin/config/services/mailigen'] = array(
    'title' => 'Mailigen',
    'description' => 'Manage Mailigen Settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailigen_admin_settings'),
    'access arguments' => array('administer mailigen'),
    'file' => 'includes/mailigen.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/services/mailigen/global'] = array(
    'title' => 'Global Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 10,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function mailigen_permission() {
  return array(
    'administer mailigen' => array(
      'title' => t('administer mailigen'),
      'description' => t('TODO Add a description for administer mailigen'),
    ),
  );
}

/**
 * Check if the given email is subscribed to the given list. Simple
 *   wrapper around mailigen_get_memberinfo().
 *
 * @param  $list_id
 * @param  $email
 *
 * @return bool
 */
function mailigen_is_subscribed($list_id, $email) {
  $subscribed = FALSE;
  $memberinfo = mailigen_get_memberinfo($list_id, $email);
  if (isset($memberinfo['status']) && $memberinfo['status'] == 'subscribed') {
    $subscribed = TRUE;
  }

  return $subscribed;
}

/**
 * Subscribe a user to a given list.
 */
function mailigen_subscribe_user($list, $email, $merge_vars, $message = TRUE, $mgapi = NULL) {
  $success = FALSE;
  if ($mgapi || $mgapi = mailigen_get_api_object()) {
    $double_optin = $list->settings['doublein'];

    $success = $mgapi->listSubscribe($list->mg_list_id, $email, $merge_vars, 'html', $double_optin, TRUE);

    if ($message && $success && $double_optin) {
      drupal_set_message(t('You have chosen to subscribe to %list. An email will be sent to your address. Click the link in the email to confirm the subscription.',
        array('%list' => $list->label)));
    }
    elseif ($message && $success) {
      drupal_set_message(t('You have subscribed to %list.', array('%list' => $list->label)));
    }

    // clear user cache, just in case there's some cruft leftover
    mailigen_cache_clear_user($list->mg_list_id, $email);

    if ($success) {
      module_invoke_all('mailigen_subscribe_user', $list, $email, $merge_vars);
      watchdog('mailigen', '@email was subscribed to list @list.',
        array('@email' => $merge_vars['EMAIL'], '@list' => $list->label), WATCHDOG_NOTICE
      );
    }
    else {
      watchdog('mailigen', 'A problem occurred subscribing @email to list @list. Message: @msg', array(
          '@email' => $merge_vars['EMAIL'],
          '@list' => $list->label,
          '@msg' => $mgapi->errorMessage,
        ), WATCHDOG_WARNING);
    }
  }

  return $success;
}

/**
 * Update a user in a given list.
 */
function mailigen_update_user($list, $email, $merge_vars, $message = TRUE, $mgapi = NULL) {
  $success = FALSE;
  if ($mgapi || $mgapi = mailigen_get_api_object()) {

    $success = $mgapi->listUpdateMember($list->mg_list_id, $email, $merge_vars);

    if ($success && $message) {
      drupal_set_message(t('You have updated your settings in %list.', array('%list' => $list->label)));
    }

    // clear user cache
    mailigen_cache_clear_user($list->mg_list_id, $email);

    if ($success) {
      watchdog('mailigen', '@email was updated in list @list.',
        array('@email' => $merge_vars['EMAIL'], '@list' => $list->label), WATCHDOG_NOTICE
      );
    }
    else {
      watchdog('mailigen', 'A problem occurred subscribing @email to list @list. Message: @msg', array(
          '@email' => $merge_vars['EMAIL'],
          '@list' => $list->label,
          '@msg' => $mgapi->errorMessage,
        ), WATCHDOG_WARNING);
    }
  }

  return $success;
}

/**
 * Unsubscribe a user from the given list.
 *
 * @param object $list
 * @param string $email
 * @param bool $message
 * @param object $mgapi
 * @param bool $delete
 *   Indicates whether an email should be deleted or just unsubscribed.
 *
 * @return bool
 */
function mailigen_unsubscribe_user($list, $email, $message = TRUE, $mgapi = NULL, $delete = FALSE) {
  $success = FALSE;
  if ($mgapi || $mgapi = mailigen_get_api_object()) {
    if (mailigen_is_subscribed($list->mg_list_id, $email)) {
      $success = $mgapi->listUnsubscribe($list->mg_list_id, $email, $delete, FALSE, FALSE);
      if ($success) {
        module_invoke_all('mailigen_unsubscribe_user', $list, $email);

        if ($message) {
          drupal_set_message(t('You have unsubscribed from %list.', array('%list' => $list->label)));
        }
      }

      // clear user cache
      mailigen_cache_clear_user($list->mg_list_id, $email);
    }
  }

  return $success;
}

/**
 * Get a Mailigen API object for communication with the mailigen server.
 */
function mailigen_get_api_object() {
  libraries_load('mailigen');
  $q = new Mailigen(variable_get('mailigen_api_key', ''));

  // set the timeout to something reasonsable to avoid taking down the Drupal site
  $q->setTimeout(60);

  // specify if a secure connection should be used wit the API
  $q->useSecure(variable_get('mailigen_use_secure', TRUE));

  if ($q->errorCode) {
    watchdog('mailigen', 'MGAPI Error: %errmsg', array('%errmsg' => $q->errorMessage), WATCHDOG_ERROR);
    return NULL;
  }

  return $q;
}

/**
 * Return all Mailigen lists for a given key. Lists are stored in the
 * cache.
 *
 * @param array $list_ids
 *   An array of list IDs to filter the results by.
 * @param bool $reset
 *   Force a cache reset.
 *
 * @return An array of list arrays.
 */
function mailigen_get_lists($list_ids = array(), $reset = FALSE) {
///  $cache = $reset ? NULL : cache_get('mailigen_lists');
  $lists = array();
  // return cached lists
  if ($cache) {
    $lists = $cache->data;
  }
  // Query lists from the MG API and store in cache
  else {
    if ($q = mailigen_get_api_object()) {
      $result = $q->lists();
      if (count($result)) {
        foreach ($result as $list) {
          // append mergefields
          $list['mergevars'] = $q->listMergeVars($list['id']);

          $lists[$list['id']] = $list;
        }
      }
    }
    uasort($lists, '_mailigen_list_cmp');

    cache_set('mailigen_lists', $lists, 'cache', CACHE_TEMPORARY);
  }

  // filter by given ids
  if (!empty($list_ids)) {
    foreach ($lists as $key => $list) {
      if (!in_array($key, $list_ids)) {
        unset($lists[$key]);
      }
    }
  }

  return $lists;
}

/**
 * Wrapper around mailigen_get_lists() to return a single list.
 *
 * @param string $list_id
 *
 * @return array list
 */
function mailigen_get_list($list_id) {
  $lists = mailigen_get_lists(array($list_id));
  return reset($lists);
}

/**
 * Get the Mailigen memberinfo for a given email address and list. Results are cached
 * in the cache_mailigen_user bin which is cleared by the MG web hooks system when
 * needed.
 *
 * @param string $list_id
 * @param string $email
 * @param bool $reset
 *
 * @return array memberinfo
 */
function mailigen_get_memberinfo($list_id, $email, $reset = FALSE) {
///  $cache = $reset ? NULL : cache_get($list_id . '-' . $email, 'cache_mailigen_user');
  $memberinfo = array();

  // return cached lists
  if ($cache) {
    $memberinfo = $cache->data;
  }
  // Query lists from the MG API and store in cache
  else {
    if ($q = mailigen_get_api_object()) {
      $result = $q->listMemberInfo($list_id, $email);
      if ($result) {
        $memberinfo = $result;
      }
    }

    cache_set($list_id . '-' . $email, $memberinfo, 'cache_mailigen_user', CACHE_PERMANENT);
  }

  return $memberinfo;
}

/**
 * Sets the member info in the cache.
 *
 * @param string $list_id
 * @param string $email
 */
function mailigen_set_memberinfo($list_id, $email) {
  mailigen_get_memberinfo($list_id, $email, TRUE);
}

/**
 * Clear a mailigen user memberinfo cache
 *
 * @param  $list_id
 * @param  $email
 */
function mailigen_cache_clear_user($list_id, $email) {
  cache_clear_all($list_id . '-' . $email, 'cache_mailigen_user');
}

/**
 * Clear a mailigen activity cache
 *
 * @param  $list_id
 */
function mailigen_cache_clear_list_activity($list_id) {
  cache_clear_all('mailigen_activity_' . $list_id, 'cache');
}

/**
 * Clear a mailigen activity cache
 *
 * @param  $list_id
 */
function mailigen_cache_clear_campaign($campaign_id) {
  cache_clear_all('mailigen_campaign_' . $campaign_id, 'cache');
}

/**
 * Implements hook_flush_caches().
 */
function mailigen_flush_caches() {
  return array('cache_mailigen_user');
}

/**
 * Helper function used by uasort() to sort lists alphabetically by name.
 *
 * @param array $a
 *   An array representing the first list.
 * @param array $b
 *   An array representing the second list.
 *
 * @return One of the values -1, 0, 1
 */
function _mailigen_list_cmp($a, $b) {
  if ($a['name'] == $b['name']) {
    return 0;
  }
  return ($a['name'] < $b['name']) ? -1 : 1;
}

/**
 * Wrapper around MGAPI::campaigns() to return data for a given campaign. Data
 * is stored in the temporary cache.
 *
 * @param $campaign_id
 *
 * @return mixed
 *   Array of campaign data or NULL if not found.
 */
function mailigen_get_campaign_data($campaign_id, $reset = FALSE) {
  $cache = $reset ? NULL : cache_get('mailigen_campaign_' . $campaign_id);
  $campaign_data = array();
  // return cached lists
  if ($cache) {
    $campaign_data = $cache->data;
  }
  else {
    $q = mailigen_get_api_object();
    $filters = array(
      'campaign_id' => $campaign_id,
    );
    $result_sandwich = $q->campaigns($filters);
    $campaign_data = $result_sandwich['data'][0];
    cache_set('mailigen_campaign_' . $campaign_id, $campaign_data, 'cache', CACHE_TEMPORARY);
  }

  return $campaign_data;
}

/**
 * Wrapper around MGAPI::campaignsForEmail() to return all IDs of campaigns
 * that have included a given email address.
 *
 * @param $email

 * @return array of campaign IDs
 */
function mailigen_get_campaigns_for_email($email) {
  $q = mailigen_get_api_object();
  $campaign_list = $q->campaignsForEmail($email);

  return $campaign_list;
}

/**
 * Returns an array of lists that the user has been a member of and
 * a value indicating whether they are currently a subscribed or unsubscribed
 *
 * @param $email
 *
 * @return array containing 2 arrays -- one ('lists') of all lists that have
 * made use of this email address (list_id -> list_data[]). One ('campaigns') of
 * all campaigns that have included this email (campaign_id -> campaign_data[])
 *
 * We include the campaign data because we need it to get accurate list
 * activity history anyway, and we want to keep the data handy to avoid
 * excessive API calls.
 */
function mailigen_get_lists_by_email($email) {
  $campaign_ids = mailigen_get_campaigns_for_email($email);
  $lists = mailigen_get_lists();
  $filtered_lists = array();
  if ($campaign_ids) {
    // iterate through campaigns, add each campaign's list as an array index/value
    foreach ($campaign_ids as $campaign_id) {
      $campaign_data = mailigen_get_campaign_data($campaign_id);
      $filtered_lists['lists'][$campaign_data['list_id']] = $lists[$campaign_data['list_id']];
      $filtered_lists['campaigns'][$campaign_id] = $campaign_data;
    }
  }

  return $filtered_lists;
}
